# remove.bg module

Project page: https://drupal.org/project/removebg

## Introduction

Remove.bg (https://www.remove.bg/) removes backgrounds from images.

## Requirements

1. Image module from Drupal core
1. [Image Effects](https://www.drupal.org/project/image_effects) module
1. API key for remove.bg

## Installing

Install as usual, see the [official documentation](https://www.drupal.org/documentation/install/modules-themes/modules-8)
for further information.

## Configuration

- Go to _Manage > Configuration > Media > remove.bg Settings_ and configure your
  API key.

## Usage

- Define image styles at _Manage > Configuration > Media > Image styles_ and add
  the "Remove background" effect.
- Make sure to also add the "Convert" effect and set it to PNG, otherwise JPG images will have a white background.
