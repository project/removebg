<?php

namespace Drupal\removebg\Plugin\ImageEffect;

use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ImageEffectBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Removes the image background.
 *
 * @ImageEffect(
 *   id = "removebg_removebg",
 *   label = @Translation("Remove background"),
 *   description = @Translation("Removes the background from the image using the remove.bg API.")
 * )
 */
class RemoveBgImageEffect extends ImageEffectBase {

    /**
     * The image factory service.
     *
     * @var \Drupal\Core\Image\ImageFactory
     */
    protected $imageFactory;

    /**
     * Constructs a RemoveBgImageEffect object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param array $plugin_definition
     *   The plugin implementation definition.
     * @param \Psr\Log\LoggerInterface $logger
     *   A logger instance.
     * @param \Drupal\Core\Image\ImageFactory $image_factory
     *   The image factory service.
     */
    public function __construct(array $configuration, $plugin_id, array $plugin_definition, LoggerInterface $logger, ImageFactory $image_factory) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);
        $this->imageFactory = $image_factory;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('logger.factory')->get('image'),
            $container->get('image.factory')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function applyEffect(ImageInterface $image) {
        $api_key = removebg_api_key();

        if (!$api_key) {
            $this->logger->error('Image could not be processed, please configure the api key for remove.bg');
            return FALSE;
        }

        $file_system = \Drupal::service('file_system');
        $tmp_file = $file_system->getTempDirectory().'/removebg_'.uniqid().'.png';
        $image->save($tmp_file);

        $client = \Drupal::httpClient();
        $res = $client->post(removebg_api_url().'/removebg', [
            'multipart' => [
                [
                    'name'     => 'image_file',
                    'contents' => fopen($tmp_file, 'r'),
                ],
                [
                    'name'     => 'size',
                    'contents' => 'auto',
                ],
            ],
            'headers' => [
                'X-Api-Key' => $api_key,
            ],
        ]);

        $fp = fopen($tmp_file, "wb");
        fwrite($fp, $res->getBody());
        fclose($fp);

        if ($res->getStatusCode() != 200) {
            $this->logger->error('Image could not be processed by remove.bg: %path', ['%path' => $image->getSource()]);
            unlink($tmp_file);
            return FALSE;
        }

        $finished_image = $image->apply('watermark', [
            'watermark_image' => $this->imageFactory->get($tmp_file),
            'watermark_width' => $image->getWidth(),
            'watermark_height' => $image->getHeight(),
            'x_offset' => 0,
            'y_offset' => 0,
            'opacity' => 100,
        ]);

        unlink($tmp_file);

        return $finished_image;
    }

}

