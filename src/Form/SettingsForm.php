<?php

namespace Drupal\removebg\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class SettingsForm extends ConfigFormBase {

    /**
     * Config settings.
     *
     * @var string
     */
    const SETTINGS = 'removebg.settings';

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'removebg_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            static::SETTINGS,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config(static::SETTINGS);

        $form['api_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('API Key'),
            '#default_value' => $config->get('api_key'),
        ];

        if (!empty($config->get('api_key'))) {
            $client = \Drupal::httpClient();
            try {
                $res = $client->get(removebg_api_url().'/account', [
                    'headers' => [
                        'X-Api-Key' => removebg_api_key(),
                    ],
                ]);
                $res_data = json_decode($res->getBody());
                if ($res->getStatusCode() == 200) {
                    $api_status = [
                        [t('Status'), 'OK'],
                        [t('Total Credits'), $res_data->data->attributes->credits->total],
                        [t('Subscription Credits'), $res_data->data->attributes->credits->subscription],
                        [t('Pay as you go Credits'), $res_data->data->attributes->credits->payg],
                        [t('Enterprise Credits'), $res_data->data->attributes->credits->enterprise],
                        [t('Free Previews via API'), $res_data->data->attributes->api->free_calls],
                        [t('API Sizes'), $res_data->data->attributes->api->sizes],
                    ];
                } else {
                    $api_status = [
                        [t('Status'), 'Error'],
                        [t('Message'), $res_data->errors[0]->title],
                    ];
                }
            } catch (\Exception $e) {
                $api_status = [
                    [t('Status'), 'Error'],
                    [t('Message'), $e->getMessage()],
                ];
            }
            $form['api_status_table'] = [
                '#type' => 'table',
                '#caption' => t('Credits and Plan'),
                '#rows' => $api_status,
            ];
        }

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $this->configFactory->getEditable(static::SETTINGS)
            ->set('api_key', $form_state->getValue('api_key'))
            ->save();

        parent::submitForm($form, $form_state);
    }

}
